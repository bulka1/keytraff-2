<?php

include "config.php";

$return_arr = array();
if (($_POST['requests']) == 2) {
    $query = "SELECT offers.name, requests.count, requests.price FROM `requests` 
LEFT JOIN `offers` ON requests.offer_id = offers.id
GROUP BY offers.name 
ORDER BY requests.count";

    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result)) {
        $name = $row['name'];
        $count = $row['count'];
        $price = $row['price'];

        $return_arr[] = array(
            "name"  => $name,
            "count" => $count,
            "price" => $price,
        );
    }
    echo json_encode($return_arr);
}else{
    $query = "SELECT requests.id, offers.name AS ofname, requests.price, requests.count, operators.name FROM `requests` 
LEFT JOIN `offers` ON requests.offer_id = offers.id
LEFT JOIN `operators` ON requests.operator_id = operators.id
WHERE requests.count > 2 AND operators.id = 10 OR operators.id = 12";

    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result)) {
        $id = $row['id'];
        $ofname = $row['ofname'];
        $name = $row['name'];
        $count = $row['count'];
        $price = $row['price'];

        $return_arr[] = array(
            "id"     => $id,
            "ofname" => $ofname,
            "count" => $count,
            "name"   => $name,
            "price"  => $price
        );
    }
    echo json_encode($return_arr);
}




/*var_dump($_POST['requests']);
$return_arr = array();
if (($_POST['request']) == 2){
    $query = "SELECT offers.name, requests.count, requests.price FROM `requests` 
LEFT JOIN `offers` ON requests.offer_id = offers.id
GROUP BY offers.name 
ORDER BY requests.count";

    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result)) {
        $name = $row['name'];
        $count = $row['count'];
        $price = $row['price'];

        $return_arr[] = array(
            "name"     => $name,
            "count" => $count,
            "price" => $price,
        );
    }
    echo json_encode($return_arr);
}else{
    $query = "SELECT requests.id, offers.name AS ofname, requests.price, requests.count, operators.name FROM `requests` 
LEFT JOIN `offers` ON requests.offer_id = offers.id
LEFT JOIN `operators` ON requests.operator_id = operators.id
WHERE requests.count > 2 AND operators.id = 10 OR operators.id = 12";

    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result)) {
        $id = $row['id'];
        $ofname = $row['ofname'];
        $name = $row['name'];
        $count = $row['count'];
        $price = $row['price'];

        $return_arr[] = array(
            "id"     => $id,
            "ofname" => $ofname,
            "count" => $count,
            "name"   => $name,
            "price"  => $price
        );
    }
}


echo json_encode($return_arr);*/