$(document).ready(function () {
  $.ajax({
    url: 'ajaxfile.php',
    type: 'get',
    dataType: 'JSON',
    success: function (response) {
      console.log(response)
      var len = response.length;
      var tr_head = "<tr>" +
        "<td>#</td>" +
        "<td>ofname</td>" +
        "<td>name</td>" +
        "<td>price</td>" +
        "<td>count</td>" +
        "</tr>";
      $("#requests thead").append(tr_head);
      for (var i = 0; i < len; i++) {
        var id = response[i].id;
        var ofname = response[i].ofname;
        var name = response[i].name;
        var price = response[i].price;
        var count = response[i].count;

        var tr_str = "<tr>" +
          "<td>" + id + "</td>" +
          "<td>" + ofname + "</td>" +
          "<td>" + name + "</td>" +
          "<td>" + price + "</td>" +
          "<td>" + count + "</td>" +
          "</tr>";

        $("#requests tbody").append(tr_str);
      }

    }
  });

  $('#req2').on('click', function () {
    $("#requests tr").remove();
    var requests = 2;
    $.ajax({
      url: 'ajaxfile.php',
      type: 'post',
      data: {requests: requests},
      dataType: 'JSON',
      success: function (response) {
        console.log(response)
        var len = response.length;
        var tr_head = "<tr>" +
          "<td>Name</td>" +
          "<td>Count</td>" +
          "<td>Price</td>" +
          "</tr>";

        $("#requests thead").append(tr_head);
        for (var i = 0; i < len; i++) {
          var name = response[i].name;
          var count = response[i].count;
          var price = response[i].price;

          var tr_str = "<tr>" +
            "<td>" + name + "</td>" +
            "<td>" + count + "</td>" +
            "<td>" + price + "</td>" +
            "</tr>";

          $("#requests tbody").append(tr_str);
        }

      }
    });
  });
});