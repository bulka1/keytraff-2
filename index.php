<!doctype html>
<html>
<head>
  <title>Test</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="jquery-3.1.1.min.js" type="text/javascript"></script>
  <script src="script.js" type="text/javascript"></script>

</head>
<body>
<div class="container" style="margin-top: 20px">
  <div class="row">
    <div class="col-md-12">
      <button class="btn btn-primary" id="req2">Request #2</button>
      <table class="table table-striped" id="requests">
        <thead>
        </thead>
        <tbody></tbody>
      </table>
    </div>
  </div>

</div>
</body>
</html>